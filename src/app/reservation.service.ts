import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { colors } from './demo-utils/colors';
import {AlertService} from './alert/alert.service';

export interface Reservation {
  id: string | number,
  title: string,
  start: Date,
  color: {
    primary: string,
    secondary: string
  },
  allDay?: boolean,
  meta?: any,
  name?: string,
  phone?: string,
  services?: string[],
  workers?: string[],
  sendSms?: boolean,
  setNotification?: boolean,
  smsSent?: boolean
}

@Injectable({
  providedIn: 'root',
})
export class ReservationService {
  private _reservations = new BehaviorSubject<Reservation[]>([]);
  private baseUrl = 'http://localhost:3337';
  private dataStore: { reservations: Reservation[] } = { reservations: [] };
  readonly reservations = this._reservations.asObservable();

  constructor(private http: HttpClient,private alertService: AlertService) { }


  success(message: string) {
    this.alertService.success(message);
  }

  error(message: string) {
    this.alertService.error(message);
  }


  loadAll() {
    this.http.get<Reservation[]>(`${this.baseUrl}/reservations`).subscribe(data => {

      console.log('LOADING DATA');
      console.log(data);
      this.dataStore.reservations = data.map(reservation => {
        return {
          ...reservation,
          start: new Date(reservation.start)
        }
      })
      /* this.dataStore.reservations = data.map((reservation: Reservation) => {
        //ura
        let strCajt = reservation.time;
        let cajtArr = []; 
        cajtArr = (strCajt || '').split(":");
        let ure: number = cajtArr[0];
        let minute: number = cajtArr[1];
        //datum
        let strDatum = reservation.date
        let datumArr = [];
        datumArr = (strDatum || '').split("/");
        let mesec: number = datumArr[1];
        let dan: number = datumArr[0];
        let zacetekStor: Date = new Date(2019,mesec-1,dan,ure,minute);
        //let konecStor: Date = new Date(leto,mesec,dan,ure+1,minute)
        return {
          id: '13',
          title: reservation.name,
          start: zacetekStor,
          color: colors.yellow,
          allDay: false,
          meta: {
            reservation
          }
        };
      });
      */
      this._reservations.next(Object.assign({}, this.dataStore).reservations);
    }, error => console.log('Could not load reservations.' + error));
  }

  load(id: number | string) {
    this.http.get<Reservation>(`${this.baseUrl}/reservations/${id}`).subscribe(data => {
      let notFound = true;

      this.dataStore.reservations.forEach((item, index) => {
        if (item.id === data.id) {
          this.dataStore.reservations[index] = data;
          notFound = false;
        }
      });

      if (notFound) {
        this.dataStore.reservations.push(data);
      }

      this._reservations.next(Object.assign({}, this.dataStore).reservations);
    }, error => console.log('Could not load reservation.' + error));
  }

  create(reservation: Reservation) {
    this.http.post<Reservation>(`${this.baseUrl}/reservations`, JSON.stringify(reservation)).subscribe(data => {
      console.log(`CREATED RESERVATION ${JSON.stringify(reservation)}, api response ${JSON.stringify(data)}`);
      this.dataStore.reservations.push(reservation);
      this._reservations.next(Object.assign({}, this.dataStore).reservations);
      console.log(data);
      this.success("Bravo majstore");
    }, error => {console.log('Could not create reservation.' + error); this.error("Napača!!!")});
  }

  update(reservation: Reservation) {
    this.http.put<Reservation>(`${this.baseUrl}/reservations/${reservation.id}`, JSON.stringify(reservation)).subscribe(data => {
      this.dataStore.reservations.forEach((t, i) => {
        if (t.id === data.id) { this.dataStore.reservations[i] = data; }
      });

      this._reservations.next(Object.assign({}, this.dataStore).reservations);
    }, error => console.log('Could not update reservation.' + error));
  }

  remove(reservationId: number) {
    this.http.delete(`${this.baseUrl}/reservations/${reservationId}`).subscribe(response => {
      this.dataStore.reservations.forEach((t, i) => {
        if (t.id === reservationId) { this.dataStore.reservations.splice(i, 1); }
      });

      this._reservations.next(Object.assign({}, this.dataStore).reservations);
    }, error => console.log('Could not delete reservation.' + error));
  }
}