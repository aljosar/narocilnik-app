import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogNewReservationComponent } from '../app/dialog-new-reservation/dialog-new-reservation.component'

import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { MatSidenav } from '@angular/material/sidenav';

import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry } from "@angular/material/icon";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
export class AppComponent {

  onResponseAlert: boolean;
  opened: boolean;
  tooltipNew = "Nova rezervacija";
  isExtraSmall: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.XSmall);
  isMedium: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Small);

  constructor(private breakpointObserver: BreakpointObserver, public dialog: MatDialog, private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIcon(
      "nslogo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/ikone/timer-logo.svg")
    );
   }


  openDialog(): void {

    
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "65%";
    dialogConfig.height = "90%";
    dialogConfig.maxWidth = "100vw";
    dialogConfig.maxHeight = "100vh";

    dialogConfig.autoFocus = false;


    const dia = this.dialog.open(DialogNewReservationComponent, dialogConfig);
    const smallDialogSubscription = this.isExtraSmall.subscribe(size => {
      if (size.matches) {
        dia.updateSize("100%", "100%");
      } else {
        dia.updateSize("72%", "90%");
      }
      dia.afterClosed().subscribe(result => {
        smallDialogSubscription.unsubscribe();

      });
    });

    const isMedium = this.isMedium.subscribe(size => {
      if (size.matches) {
        dia.updateSize("100%", "100%");
      } 
      dia.afterClosed().subscribe(result => {
        isMedium.unsubscribe();

      });
    });

    // const mojDiaSub = this.isModdle.subscribe(size =>{
    //   if(size.matches) {
    //     dia.updateSize("100%","100%");
    //   }

    //   dia.afterClosed().subscribe(result =>{
    //     console.log(result);
    //     mojDiaSub.unsubscribe();
    //   });
    // });
  }
}
