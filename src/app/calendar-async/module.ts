import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { DemoUtilsModule } from '../demo-utils/module';
import { DemoComponentAsync } from './component';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import lokalc from '@angular/common/locales/sl';

registerLocaleData(lokalc);

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    DemoUtilsModule
  ],
  declarations: [DemoComponentAsync],
  exports: [DemoComponentAsync]
})
export class DemoModuleAsync {}
