import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
// import * as requestPromise from 'request-promise';
import { map } from 'rxjs/operators';
import { CalendarEvent, CalendarDateFormatter } from 'angular-calendar';
import {
  isSameMonth,
  isSameDay,
  startOfMonth,
  endOfMonth,
  startOfWeek,
  endOfWeek,
  startOfDay,
  endOfDay,
  format
} from 'date-fns';
import { Observable, Subscription } from 'rxjs';
import { colors } from '../demo-utils/colors';
import {CustomDateFormatter} from './custom-date-formatter.provider';
import { TestService } from '../test.service'; 
import { ReservationService } from '../reservation.service';

interface Reservation {
  name: string,
  phone: string,
  services: string[],
  date: string,
  time: string,
  workers: string[],
  sendSms: boolean,
  setNotification: boolean,
  smsSent: boolean,
  start: Date,
  title: string
}



@Component({
  selector: 'mwl-async-demo-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'template.html',
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ]
})
export class DemoComponentAsync implements OnInit {
  view: string = 'month';
  locale: string = 'sl';
  viewDate: Date = new Date();
  test: string;
  
  
  reservations$:  Observable<Array<any>>;
  subscription: Subscription;
  reservations: any;
 
  activeDayIsOpen: boolean = false;

  constructor(
    private http: HttpClient,
    private ts: TestService,
    private reservationService: ReservationService
  ) { }

  ngOnInit(): void {
    this.test = this.ts.testFunction();
    this.reservations = this.reservationService.reservations;
    this.reservations$ = this.reservationService.reservations;
    this.reservationService.loadAll();
  }
  
  ngOnDestroy() { }

  dayClicked({
    date,
    events
  }: {
    date: Date;
    events: Array<CalendarEvent<{ reservation: Reservation }>>;
  }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventClicked(event: CalendarEvent<{ reservation: Reservation }>): void {
    console.log(`Clicked`);
  }

  onClickTest() {
    console.log(this.reservations$);
    this.reservationService.create({
      id: 'on click add',
      title: 'on click add',
      start: new Date(2019,10,4,15,30),
      color: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
      },
      allDay: false,
      meta: {
        reservation: 'bla'
      }
    });
    this.reservationService.loadAll();
    console.log(this.reservations$);
  }
}