import { Injectable } from '@angular/core';
import { of, Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CalendarEvent } from 'angular-calendar';
import { colors } from './demo-utils/colors';

interface Reservation {
  name: string,
  phone: string,
  services: string[],
  date: string,
  time: string,
  workers: string[],
  sendSms: boolean,
  setNotification: boolean,
  smsSent: boolean,
  start: Date,
  title: string
}

@Injectable({
  providedIn: 'root',
})
export class TestService {
  // TODO: create services for everything: managing database, updating reservations, providing reservations, etc.
  reservations: any[] = [];
  subject: Subject<any> = new Subject<any>();
  
  constructor(private http: HttpClient) { }

  testFunction() {
    return 'TEST from service';
  }
}
