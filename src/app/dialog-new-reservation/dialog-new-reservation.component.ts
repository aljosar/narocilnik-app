import { Component, OnInit, Input, Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Rezervacija } from './rezervacija';
import { Observable } from '../../../node_modules/rxjs';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { TestService } from '../test.service';
import { ReservationService } from '../reservation.service';

@Component({
  selector: 'app-dialog-new-reservation',
  templateUrl: './dialog-new-reservation.component.html',
  styleUrls: ['./dialog-new-reservation.component.css']
})
export class DialogNewReservationComponent implements OnInit {
  @Output() eventEmitter = new EventEmitter<any>();
  //tooltipi
  tooltipSend = "Oddaj rezervacijo";
  tooltipClose = "Zapri";
  //
  readonly url = 'http://localhost:3337/reservations';
  novaRezervacija: Observable<any>;
  imeStranke: string;
  telStranke: string;
  datePckr: any = {};
  dan: string;
  mesc: string;
  leto: string;
  datumStor: string;
  time = { hour: 13, minute: 30 };
  urca: string;
  minuta: string;
  uraStor: string;

  sms: boolean;
  opomnik: boolean;


  alert: boolean;

  //storitve
  dataStor = [];
  selectedItemsStor = [];
  settingsStor = {};
  izbraneStoritveKoncna: string[] = [];
  strStor: string;

  //delavci
  data = [];
  selectedItems = [];
  izbraniDelavci: string[] = [];
  strDevovc: string;
  settings = {};

  izbranRadioSms = 0;
  izbranRadioOpomnik = 0;

  constructor(
    private httpClient: HttpClient,
    private dialogRef: MatDialogRef<DialogNewReservationComponent>,
    private reservationService: ReservationService
  ) { }

  zapriModal() {
    this.dialogRef.close();
  }

  postRezervacija() {

    // nardi string datuma storitve dd/mm/yyyy
    this.dan = this.datePckr.day;
    this.mesc = this.datePckr.month;
    this.leto = this.datePckr.year;
    this.datumStor = this.dan + "/" + this.mesc + "/" + this.leto;

    //nardi string ura storitve hh:mm
    this.urca = this.time.hour.toString();
    this.minuta = this.time.minute.toString();
    this.uraStor = this.urca + ":" + this.minuta;


    //a nastavim opomnik...
    if (this.izbranRadioOpomnik == 1) {
      this.opomnik = true;
    }
    if (this.izbranRadioOpomnik == 2) {
      this.opomnik = false;
    }
    if (this.izbranRadioOpomnik == 0) {
      console.log("niste nastavili opcije opomnika...");
      this.opomnik = false;
    }
    //a pošljem sms stranki radio group...
    if (this.izbranRadioSms == 1) {
      this.sms = true;
    }
    if (this.izbranRadioSms == 2) {
      this.sms = false;
    }
    if (this.izbranRadioSms == 0) {
      console.log("niste izbrali ali želite poslati sms opcije...")
      this.sms = false;
    }

    for (let stor of this.selectedItemsStor) {
      this.strStor = stor.item_text;
      this.izbraneStoritveKoncna.push(this.strStor);
    }

    for (let devalc of this.selectedItems) {
      this.strDevovc = devalc.item_text;
      this.izbraniDelavci.push(this.strDevovc);
    }

    const rez: Rezervacija = {
      id: "id reza",
      color: {
        primary: "#D13D3D",
        secondary: "#D13D3D"
      },
      title: this.imeStranke,
      name: this.imeStranke,
      phone: this.telStranke,
      services: this.izbraneStoritveKoncna,
      workers: this.izbraniDelavci,
      start: new Date(2019, +this.mesc - 1, +this.dan, +this.urca, +this.minuta),
      sendSms: this.sms,
      setNotification: this.opomnik,
      smsSent: false
    }

    // TODO: dodaj v lokalno tabelo da se prikaze na koledarju: spremeni date v pravilen format
    this.reservationService.create(rez);
    this.reservationService.loadAll();
    // zapri modal 
    this.zapriModal();
    // TODO: prikazi load page,

    //POST reservation to api
    /*let jsonRez = JSON.stringify(rez);
    this.novaRezervacija = this.httpClient.post(this.url, jsonRez);
    this.novaRezervacija.subscribe(
      (value) => {
        console.log(`OnSuccess: ${JSON.stringify(value)}`);
        // TODO: prikazi success notification
      },
      (error) => {
        console.log(`OnError: ${JSON.stringify(error)}`);
        // TODO: prikazi error notification
      },
      () => {
        console.log(`Always`);
        // TODO: zapri load page, 
      }
    );*/
  }


  ngOnInit() {

    this.alert = false;

    this.data = [
      { item_id: 1, item_text: 'Vesna' },
      { item_id: 2, item_text: 'Simla' },
      { item_id: 3, item_text: 'Sara' }
    ];
    this.settings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      enableCheckAll: true,
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 3,
      selectAllText: 'Izberi vse',
      unSelectAllText: 'Izberi vse',
      searchPlaceholderText: 'Iskanje',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    };

    this.dataStor = [
      { item_id: 1, item_text: 'per.lak roke' },
      { item_id: 2, item_text: 'naravna manikura' },
      { item_id: 3, item_text: 'pedikura' },
      { item_id: 4, item_text: 'pedikura + lak' },
      { item_id: 5, item_text: 'depilacija' },
      { item_id: 6, item_text: 'nega obraza' },
      { item_id: 7, item_text: 'oblikovanje obrvi' },
      { item_id: 8, item_text: 'kavitacija' },
      { item_id: 9, item_text: 'limfna drenaža' },
      { item_id: 10, item_text: 'radiofrekvenca' },
      { item_id: 11, item_text: 'masaža' },
      { item_id: 12, item_text: 'laser' },
    ];

    this.settingsStor = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      enableCheckAll: true,
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 3,
      selectAllText: 'Izberi vse',
      unSelectAllText: 'Izberi vse',
      searchPlaceholderText: 'Iskanje',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    };
  }

}
