export interface Rezervacija {
    id: string
    color: {
        primary: string,
        secondary: string
    },
    title: string
    name: string
    phone: string
    services:any[]
    workers:any[]
    start:Date
    sendSms:boolean
    setNotification:boolean
    smsSent: boolean 
}