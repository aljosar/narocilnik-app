import { Component, OnInit }                               from '@angular/core';
import { ActivatedRoute, Router }                          from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// import { SystemService }                                   from '../../../core/system/system.service';
// import { UserService }                                     from '../../../core/auth/user.service';

@Component({
  selector: 'app-lockscreen',
  templateUrl: './lockscreen.component.html',
  styleUrls: ['./lockscreen.component.scss']
})
export class LockscreenComponent implements OnInit {

  public returnUrl: string;

  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    // private sys: SystemService,
    // private userService: UserService,
  ) {}

  ngOnInit() {
    this.form = this.fb.group ( {
      // username: [ this.userService.user.username, Validators.compose( [ Validators.required ] ) ],
      // password: [null, Validators.compose([Validators.required])]
    } );

    // this.route.queryParams
    //   .subscribe(params => {
    //     this.returnUrl = params.returnUrl;
    //   });
  }

  onSubmit() {
    // this.sys.auth.signIn(this.form.value)
    //   .subscribe(
    //     res => this.router.navigate([this.returnUrl || '/']).catch(x => this.sys.handleError(x)),
    //     err => this.sys.handleError(err)
    //   );
  }

}
