import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  //clientInfo = staticClientInfo;
  
  forgotPasswordData = {
    identifier: ''
  };
  error: any;
  errorMsg: string;
  resetSuccessful: boolean;

  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    //private router: Router,
    //private sys: SystemService,
    //private snackBar: MatSnackBar,
    //private notificationsService: NotificationsService,
    //private pageDataService: JsfPageDataService
  ) {
    this.form = this.fb.group({
      identifier: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit(): void {
   // this.pageDataService.setWindowTitle(['Forgot password']);
  }

  onSubmit() {
    // this.forgotPasswordData.identifier = this.form.get('identifier').value;

    // this.sys.auth.forgotPassword(this.forgotPasswordData)
    //   .subscribe( (promise) => {
    //       this.resetSuccessful = true;
    //       this.notificationsService.success('We sent further instructions to an email address associated with the provided account, if it exists.', 'Success');
    //     },
    //     err => {
    //       this.error = err;
    //       this.resetSuccessful = false;
    //     });
  }
}
