import { Component, OnInit }                               from '@angular/core';
import { ActivatedRoute, Router }                          from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
// import { SystemService }                                   from '../../../core/system/system.service';
// import { staticClientInfo }                                from '../../../core/client/client.static';
// import { NotificationsService }                            from '../../../core/notifications/notifications.service';
// import { JwtHelperService }                                from '@auth0/angular-jwt';
// import { JsfPageDataService }                              from '../../../services/jsf-page-data.service';

// const jwtHelper = new JwtHelperService();


@Component({
  selector   : 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls  : ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {

  // clientInfo = staticClientInfo;

  public form: FormGroup;

  public returnUrl: string;
  public token: string;
  public tokenData: any;
  public ref: string;

  wrongCredentials: boolean;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    //private sys: SystemService,
    //private notificationsService: NotificationsService,
    //private pageDataService: JsfPageDataService
  ) {
    //this.sys.logout().catch(x => this.sys.handleErrorNoInteraction(x));
  }

  ngOnInit() {
    // this.route.queryParams
    //   .subscribe(params => {
    //     this.returnUrl = params.returnUrl;

    //     this.ref       = params.ref;
    //     this.token     = params.token;
    //     this.tokenData = this.token && jwtHelper.decodeToken(this.token);

        this.form = this.fb.group({
          username       : [''],
          password       : ['', Validators.compose([
            Validators.required,
            this.validatePasswordComplexity.bind(this),
          ])],
          confirmPassword: ['', Validators.compose([
            Validators.required,
            this.validateConfirmPasswordMatches.bind(this),
          ])],
        });

    //     this.form.get('username').disable();
    //     if (this.tokenData && this.tokenData.username) {
    //       this.form.get('username').setValue(this.tokenData.username);
    //     }
    //   });

    // this.pageDataService.setWindowTitle(['Reset password']);
  }

  onSubmit() {
    this.wrongCredentials = false;

    // this.sys.auth.changePasswordWithToken({ token: this.token, password: this.form.value.password })
    //   .subscribe(
    //     res => {
    //       this.notificationsService.success('Password changed. Please log in to continue.', 'Success');
    //       this.router.navigate([this.returnUrl || '/']).catch(x => this.sys.handleError(x));
    //     },
    //     err => {
    //       this.sys.handleError(err);
    //     },
    //   );
  }

  validatePasswordComplexity(c: FormControl) {
    const passwordRegex = /^(?=.*?[0-9]).{8,}$/; // At least 1 number, min 8 chars in length

    return passwordRegex.test(c.value) ? null : {
      passwordComplexity: {
        valid: false,
      },
    };
  }

  validateConfirmPasswordMatches(c: FormControl) {
    if (!this.form) {
      return null;
    }

    return this.form.get('password').value === c.value ? null : {
      passwordMatches: {
        valid: false,
      },
    };
  }

}
