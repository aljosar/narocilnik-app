import { Component, OnInit }                  from '@angular/core';
import { ActivatedRoute, ParamMap, Router }   from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { SystemService }                      from '../../../core/system/system.service';
// import { staticClientInfo }                   from '../../../core/client/client.static';
// import { api }                                from '../../../core/api/api.util';
// import { JsfPageDataService }                 from '../../../services/jsf-page-data.service';

@Component({
  selector   : 'app-login',
  templateUrl: './login.component.html',
  styleUrls  : ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //clientInfo = staticClientInfo;

  isAdminMode = false;

  public form: FormGroup;

  public returnUrl: string;

  wrongCredentials: boolean;

  constructor(
        private fb: FormBuilder,
  //   private router: Router,
  //   private route: ActivatedRoute,
  //  //private sys: SystemService,
  //   private pageDataService: JsfPageDataService
  ) {
    // this.sys.logout().catch(x => this.sys.handleErrorNoInteraction(x));
  }

  ngOnInit() {
    this.form = this.fb.group({
      username: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
  //   this.route.queryParams
  //     .subscribe(params => {
  //       this.returnUrl = params.returnUrl;
  //       this.isAdminMode = params.mode === 'admin';

  //       /**
  //        * Only used as dev shortcut!!! there is no prod usage for this and there should not be any!
  //        */
  //       if (params.u && params.p) {
  //         console.warn('AUTO LOGIN used! (only usage should be from fast client switcher)');
  //         this.form.setValue({
  //           username: params.u,
  //           password: params.p,
  //         });
  //         this.onSubmit();
  //       }
  //     });

  //   this.pageDataService.setWindowTitle(['Login']);
  }

  onSubmit() {
    // this.wrongCredentials = false;
    // (this.isAdminMode ? this.sys.auth.signInAsAdmin(this.form.value) : this.sys.auth.signIn(this.form.value))
    //   .subscribe(
    //     res => {
    //       if (this.isAdminMode) {
    //         this.router.navigate([this.returnUrl || '/admin']).catch(x => this.sys.handleError(x));
    //       } else {
    //         this.router.navigate([this.returnUrl || '']).catch(x => this.sys.handleError(x));
    //       }
    //     },
    //     err => {
    //       if (err.status === 401) {
    //         this.wrongCredentials = true;
    //         this.form.controls['password'].reset();
    //       } else {
    //         this.sys.handleError(err);
    //       }
    //     }
    //   );
  }
}
