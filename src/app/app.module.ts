import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { AppComponent } from './app.component';
import { DemoComponentAsync } from './calendar-async/component';
import { DemoModuleAsync } from './calendar-async/module';
import { DemoUtilsModule } from './demo-utils/module';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DialogNewReservationComponent } from './dialog-new-reservation/dialog-new-reservation.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { NgMultiSelectDropDownModule } from 'node_modules/ng-multiselect-dropdown';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { MatCardModule} from '@angular/material/card';
import { ForgotComponent } from './auth-components/forgot/forgot.component';
import { MatInputModule } from '@angular/material';
import { LoginComponent } from './auth-components/login/login.component'
import {MatCheckboxModule} from '@angular/material/checkbox';
import { RegisterComponent } from './auth-components/register/register.component';
import { ResetPasswordComponent } from './auth-components/reset-password/reset-password.component';
import { LockscreenComponent } from './auth-components/lockscreen/lockscreen.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import 'hammerjs';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogNewReservationComponent,
    ForgotComponent,
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent,
    LockscreenComponent,
    AlertComponent
  ],
  entryComponents: [DialogNewReservationComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatDialogModule,
    MatRadioModule,
    MatGridListModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgbModule,
    HttpClientModule,
    DemoModuleAsync,
    DemoUtilsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatDividerModule,
    MatInputModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatCardModule,
    RouterModule.forRoot([
      {
        path: 'forgot', 
        component: ForgotComponent,
        pathMatch: 'full'

      },
      {
        path: 'login', 
        component: LoginComponent,
        pathMatch: 'full'
      },
      {
        path: 'register', 
        component: RegisterComponent,
        pathMatch: 'full'
      },
      {
        path: 'reset-password', 
        component: ResetPasswordComponent,
        pathMatch: 'full'
      },
      {
        path: 'lockscreen', 
        component: LockscreenComponent,
        pathMatch: 'full'
      }

    ]),
    

  ],
  providers: [],
  bootstrap: [
    AppComponent,
    DemoComponentAsync
  ]
})
export class AppModule { }
